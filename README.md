composer install

Put db info in .env
bin/console doctrine:databasew:create
bin/console doctrine:schema:update 

Run data fixtures
bin/console doctrine:fixtures:load

Start server:
bin/console server:run

Request a token via cookie:

    axios.post(`http://localhost:8000/api/login_check`, {
        username: this.state.username,
        password: this.state.password
    }, 
    {
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })

Since token is stored in cookie it is automatically returned 
and validated on every request

Catching expired tokens and issuing refresh token via cookie

    const instance = axios.create();
    let attempted = false;
    
    instance.defaults.withCredentials = true;
    
    instance.interceptors.response.use(response => {
        return response;
      }, error => {
        const { config, response: { status } } = error;
        const originalRequest = config;
      
        if (status === 401) {
    
            if(!attempted){
                attempted = true;
                let refreshCall = handleRefreshTokenRequested();
    
                refreshCall.then(res => {
                    if(res){
                        attempted = false;
                        return instance.request(originalRequest);
                    }
                });
            }
            
        }else{
            attempted = false;
        }
    
        
      });
    
      const handleRefreshTokenRequested = (cb) => {
        const refreshCall = instance.post(`http://localhost:8000/api/token/refresh`)
        .then(cb);
    
        return refreshCall;
    }

Intercepts invalid access token, requests new token with refresh token and
replays original request keeping the user logged in with new 
credentials.