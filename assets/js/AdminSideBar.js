class AdminSideBar{

  constructor($elm)
  {
    this.sideBar = $elm;

    this.sideBar.hover(this._expandSideBar.bind(this), this._contractSideBar.bind(this));
  }

  _expandSideBar(){
    this.sideBar.find('.on-sidebar-expansion').toggleClass('hidden');
  }

  _contractSideBar(){
    this.sideBar.find('.on-sidebar-expansion').toggleClass('hidden');
  }

}

export default AdminSideBar;