import 'materialize-css/sass/materialize.scss';
import '../css/admin.scss';

import 'materialize-css/dist/js/materialize.js';
import 'materialize-css/js/initial.js';

import AdminSideBar from "./AdminSideBar";

new AdminSideBar($('#admin-main-sidebar'));