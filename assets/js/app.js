// assets/js/app.js

// loads the jquery package from node_modules
// var $ = require('jquery');

// import the function from greet.js (the .js extension is optional)
// ./ (or ../) means to look for a local file
// var greet = require('./greet');

import 'materialize-css/sass/materialize.scss';
import '../css/app.scss';

import 'materialize-css/dist/js/materialize.js';
import 'materialize-css/js/initial.js';


$(document).ready(function() {

  $(".button-collapse").sideNav();

});