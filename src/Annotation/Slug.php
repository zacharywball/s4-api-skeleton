<?php
/**
 * Created by PhpStorm.
 * User: zball
 * Date: 6/28/18
 * Time: 12:50 PM
 */

namespace App\Annotation;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Slug
{
    /** @var array<string> @Required */
    public $fields = array();
}