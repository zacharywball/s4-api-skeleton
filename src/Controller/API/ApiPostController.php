<?php
/**
 * Created by PhpStorm.
 * User: zac
 * Date: 9/20/18
 * Time: 6:56 PM
 */

namespace App\Controller\API;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api")
 */
class ApiPostController extends Controller
{
    /**
     * @Route("/test")
     */
    public function test(Request $request)
    {
        return new JsonResponse(['user' => $this->getUser()->getUsername()], 200);
    }
}