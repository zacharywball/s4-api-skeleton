<?php
/**
 * Created by PhpStorm.
 * User: zball
 * Date: 3/9/18
 * Time: 10:52 PM
 */

namespace App\Controller;

use App\Entity\ExternalLinkPost;
use App\Entity\VideoPost;
use App\Entity\WrittenPost;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $writtenPostRepo = $em->getRepository(WrittenPost::class);
        $externalLinkRepo = $em->getRepository(ExternalLinkPost::class);
        $videoPostRepo = $em->getRepository(VideoPost::class);

        $featuredExternalLinks = $externalLinkRepo->getFeatured();
        $featuredExternalLinks =  array_chunk( $featuredExternalLinks , 6 );

        $topRankedWrittenPost = $writtenPostRepo->getTopRanked();

        $topRankedVideoPosts = $videoPostRepo->getFeatured(6);

        $allFeaturedExceptTopRankedPosts = $writtenPostRepo->getAllFeaturedExceptTopRanked(3);

        $headlines = $externalLinkRepo->getLatestNonFeatured(40);
        $headlines = array_chunk( $headlines, 10 );

        return $this->render('index.html.twig', [
            'featuredExternalLinks' => $featuredExternalLinks,
            'topRankedWrittenPost' => $topRankedWrittenPost,
            'remainingFeaturedWrittenPosts' => $allFeaturedExceptTopRankedPosts,
            'headlines' => $headlines,
            'topRankedVideoPosts' => $topRankedVideoPosts
        ]);
    }

}