<?php
/**
 * Created by PhpStorm.
 * User: zac
 * Date: 9/18/18
 * Time: 12:15 AM
 */

namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $objectManager)
    {
        $user = ( new User() )
            ->setUsername('zball')
            ->setEmail('zball@gmail.com')
            ->setIsActive(true);

        $password = $this->encoder->encodePassword($user, '1qaz');

        $user->setPassword($password);

        $objectManager->persist($user);
        $objectManager->flush();
    }
}